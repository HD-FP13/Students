# specify compiler and flags
CXX = c++
CXXFLAGS += -Wall -O2 -g

# build with all root libs
ROOTCFLAGS	 = $(shell root-config --cflags)
ROOTLIBS	 = $(shell root-config --libs)
CXXFLAGS	+= $(ROOTCFLAGS)
LDFLAGS		+= $(ROOTLIBS)

# Get source files and object names

SRC = $(wildcard *.cc)
OBJ = $(SRC:%.cc=%.o)

# just calling make will build fp13
all: fp13

# clean up: remove old object files and the like
clean:
	rm -f *.o fp13



# specify the dependencies of the files - make will figure out the rest
fp13:	$(OBJ)
	$(CXX) $^ -o $@ $(LDFLAGS)

%.o:	%.cc %.h
	$(CXX) $(CXXFLAGS) -c $< -o $@